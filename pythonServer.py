import socketserver
from http.server import HTTPServer, SimpleHTTPRequestHandler


paths = {'/': 'index.html', '/about': 'about.html', '/contact': 'contact.html'}


class StandbyServer(SimpleHTTPRequestHandler):
    def getpath(self):
        if self.path == '/' or self.path == '/index' or self.path == '/about' or self.path == '/contact' \
                or self.path == '/index.html' or self.path == '/about.html' or self.path == '/contact.html':
            content = self.handle_http(200, 'text/html')
            self.wfile.write(content)
        else:
            content = self.handle_http(404, 'text/plane')
            self.wfile.write(content)

    def handle_http(self, status, contentType):
        self.send_response(status)
        self.end_headers()
        if status == 200:
            path_content = paths[self.path]
            open_page = open(path_content, 'r')
        elif status == 404:
            content = self.handle_http(404, 'text/plain')
            self.wfile.write(content)

        return bytes(open_page.read(), 'UTF-8')


def run(server_class=HTTPServer, handler_class=StandbyServer):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


"""def serverup(server_class=HTTPServer, handlerclass=StandbyServer, PORT=8080):
    #startServer = ("", port)
    #httpd = server_class(startServer, handlerclass)
    httpd = server_class(socketserver.TCPServer(("", PORT), handlerclass, RequestHandlerClass))
    print("serving at port", PORT)
    httpd.serve_forever()"""


run()