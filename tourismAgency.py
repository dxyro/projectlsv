class tourismAgency:
    def __init__(self, tAgCode, name, direction):
        self.tAgCode = tAgCode
        self.name = name
        self.direction = direction
        self.driver_list = []
        self.tour_list = []

        agency = {'tAgCode': self.tAgCode, 'name': self.name, 'direction': self.direction,
                  'driver_list': self.driver_list, 'tour_list': self.tour_list}

        '''for key, value in agency.items():
            temp = [key, value]
            agencys_list.append(temp)'''

        agencys_list.append(agency)


    def _str_(self) -> str:
        return f'tAgCode: {self.tAgCode} ---- Nombre: {self.name} ---- Apellido: {self.direction} ---- Lista de conductores: {self.driver_list}'

class person:
    def __init__(self, id, name, lastName, nationality, age, gender):
        self.id = id
        self.name = name
        self.lastName = lastName
        self.nationality = nationality
        self.age = age
        self.gender = gender

        person = {'Nombre': self.name, 'Identificacion': self.id, 'Apellido': self.lastName,
                  'Nacionalidad': self.nationality, 'Edad': self.age, 'Gender': self.gender}

        persons_list.append(person)

    def _str_(self) -> str:
        return f'Identificacion: {self.id} ---- Nombre: {self.name} ---- Apellido: {self.lastName} ---- Nacionalidad: {self.nationality}' \
            f' ---- Edad: {self.age} ---- Genero: {self.gender}'


class drivers(person, tourismAgency):
    def __init__(self, id, name, lastName, nationality, age, gender, driverCode, tAgCode):
        person.__init__(self, id, name, lastName, nationality, age, gender)
        self.driverCode = driverCode
        self.tAgCode = tAgCode

        driver = {'name': self.name, 'id': self.id, 'lastName': self.lastName,
                  'nationality': self.nationality, 'age': self.age, 'gender': self.gender,
                  'driverCode': self.driverCode, 'tAgCode': self.tAgCode}
        drivers_list.append(driver)

    def _str_(self) -> str:
        return f'Identificacion: {self.id} ---- Nombre: {self.name} ---- Apellido: {self.lastName} ---- Nacionalidad: {self.nationality}' \
            f' ---- Edad: {self.age} ---- Genero: {self.gender} ---- Codigo de agencia: {self.tAgCode}'


class turist(person):
    def __init__(self, id, name, lastName, nationality, age, gender, tourCode):
        person.__init__(self, id, name, lastName, nationality, age, gender)
        self.tourCode = tourCode

        turist = {'name': self.name, 'id': self.id, 'lastName': self.lastName,
                  'nationality': self.nationality, 'age': self.age, 'gender': self.gender, 'tourCode': self.tourCode}

        turist_list.append(turist)


class tours:
    def __init__(self, cod, destiny, date, cost, duration, driverCode, tAgCode):
        self.cod = cod
        self.destiny = destiny
        self.date = date
        self.cost = cost
        self.duration = duration
        self.dirverCode = driverCode
        self.tAgCode = tAgCode
        self.turist_list = []

        tour = {'cod': self.cod, 'destiny': self.destiny, 'date': self.date,
                'cost': self.cost, 'duration': self.duration, 'dirverCode': self.dirverCode,
                'tAgCode':self.tAgCode, 'turist_list': self.turist_list}

        tours_list.append(tour)


agencys_list = []
persons_list = []
drivers_list = []
turist_list = []
tours_list = []
agencys_dict = []
persons_dict = []
drivers_dict = []
turist_dict = []
tours_dict= []


tour1 = tours(cod=123, destiny='Cartagena', date='12/11/19', cost=1234234,duration=2, driverCode=123, tAgCode=213)
tour2 = tours(cod=1234, destiny='Bogota', date='1/11/19', cost=12342334,duration=12, driverCode=214, tAgCode=213)
tour3 = tours(cod=12345, destiny='Barranquilla', date='1/9/19', cost=12542334,duration=4, driverCode=214, tAgCode=214)

tourismAgency(tAgCode =213,name='Dxyro', direction='kr13 #sad')
tourismAgency(tAgCode =14,name='Jxvier', direction='kr13 #5-2')

turist(id=1, name="Juan", lastName="Vergara", nationality="colombiano", age=23, gender="M", tourCode=123)
turist(id=2, name="Pedro", lastName="Perez", nationality="Mexicano", age=30, gender="M", tourCode=123)
turist(id=3, name="Luis", lastName="Andrades", nationality="Americano", age=50, gender="M",  tourCode=1234)

drivers(id=5123456, name="Damien", lastName="chamorro", nationality="Thorn", age=15, gender="M", driverCode=11234, tAgCode=213)
drivers(id=543, name="dsfg", lastName="chamorror", nationality="hgf", age=18, gender="F", driverCode=134, tAgCode=213)
drivers(id=1234, name="Dxyro", lastName="chamorro", nationality="hgf", age=20, gender="M", driverCode=11234, tAgCode=213)
drivers(id=1531, name="Dairo", lastName="Barrios", nationality="colombiano", age=26, gender="M", driverCode=105242, tAgCode=214)


def change_dic_to_list(dic_to_change):
    dictlist = []
    '''#for agIdx, agRow in enumerate(agencys_list):
    for key, value in dic_to_change.items():
        temp = value
        dictlist.append(temp)
    return dictlist'''
    return dic_to_change


def add_list_in_list(father, son, equalf, equals, list_name_to_add):
    for agIdx, agRow in enumerate(father):
        for drIdx, drivRow in enumerate(son):
            if father[agIdx].get(equalf) == son[drIdx].get(equals):
                father[agIdx].get(list_name_to_add).append(change_dic_to_list(son[drIdx]))
    return father




all_in_one = []
agencys_list = add_list_in_list(agencys_list, drivers_list, 'tAgCode', 'tAgCode', 'driver_list')
tours_list = add_list_in_list(tours_list, turist_list, 'cod', 'tourCode', 'turist_list')
agencys_list = add_list_in_list(agencys_list, tours_list, 'tAgCode', 'tAgCode', 'tour_list')

for agIdx, agRow in enumerate(agencys_list):
    all_in_one.append(change_dic_to_list(agencys_list[agIdx]))


#print(agencys_list)

duration =0

for tourIdx, tourRow in enumerate(tours_list):
    if duration < tours_list[tourIdx].get('duration'):
        duration = tours_list[tourIdx].get('duration')
        aux = tours_list[tourIdx]

print('Total de conductores ', len(drivers_list))
print('Total de Turistas ', len(turist_list))
print('Total de personas ', len(persons_list))

print(all_in_one)


#change_dic_to_list(agencys_list)


#print(sorted(all_in_one.iteritems(), key = lambda x : x[1]))


#all_in_one.sort(key=lambda desc: desc[0][1], reverse=True)

#print(all_in_one)

