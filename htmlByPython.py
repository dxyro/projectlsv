import requests
import json
photos = []
users = []
todos = []
comments = []
urlp = requests.get('https://jsonplaceholder.typicode.com/photos')
urlu = requests.get('https://jsonplaceholder.typicode.com/users')
urlt = requests.get('https://jsonplaceholder.typicode.com/todos')
urlc = requests.get('https://jsonplaceholder.typicode.com/comments')
photos = json.loads(urlp.text)
users = json.loads(urlu.text)
todos = json.loads(urlt.text)
comments = json.loads(urlc.text)
html = open('LSV-TEAM.html', 'w')
mensaje = """<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>LSV-TEAM</title>
    
</head>
<body>
    <div class="container">
        <br/><br/>
        <h1 class="text-center">LSV-TEAM</h1>
        <br/><br/>
        <h4>Photos</h4>
        <div class="row">"""
mensaje += ''
for i in photos[0:8]:
    mensaje += '''
    <div class="col-3" >
              <img src="''' + i['url']+'''" alt="''' + i['title']+'''" class="img-thumbnail">
            </div>
    '''
mensaje += '''
        </div>
        <br/><br/>
        <h4>Users</h4>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Usuario</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Ciudad</th>
                </tr>
            </thead>
        <tbody>
            
'''
for i in users[0:10]:
    mensaje += '''
            <tr>
                <th scope="row">''' + str(i['id'])+'''</th>
                <td>''' + i['name']+'''</td>
                <td>''' + i['username']+'''</td>
                <td>''' + i['email']+'''</td>
                <td>''' + i['address']['city']+'''</td>
            </tr>'''
mensaje += '''
           </tbody>
        </table>
        <br/><br/>
        <h4>To Do</h4>
        <table class="table">
            <thead class="thead-light">
                <tr>
                <th scope="col">#</th>
                <th scope="col">Usuario</th>
                <th scope="col">Título</th>
                <th scope="col">Completado</th>
                </tr>
            </thead>
          <tbody>'''
for i in todos[0:10]:
    if i['completed']:
        i['completed'] = 'Si'
    else:
        i['completed'] = 'No'
    for j in users[0:10]:
        if i['userId'] == j['id']:
            i['userId'] = j['name']
    mensaje += '''
              <tr>
              <th scope="row">''' + str(i['id'])+'''</th>
              <td>''' + i['userId']+'''</td>
              <td>''' + i['title']+'''</td>
              <td>''' + i['completed'] + '''</td>
            </tr>'''
mensaje += '''
          </tbody>
        </table>
        <br/><br/>
        <h3>Comentarios</h3>
        <div class="row">'''
for i in comments[0:4]:
    mensaje += '''
        
            <div class="col-12">
              <p class="bg-dark padding"><strong class="color">''' + str(i['id'])+'''</strong></p>
              <p>''' + i['name']+'''</p>
              <p>''' + i['email']+'''</p>
              <p class="bd-callout-info">''' + i['body']+'''</p>
            </div>'''
mensaje += '''
        </div>
    </div>
</body>
</html>'''
html.write(mensaje)
html.close()