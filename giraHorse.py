chessboard = []
for i in range(8):
    chessboard.append([0]*8)
count = 1


def main():
    global chessboard
    global count
    # Estableciendo la pocion
    print('Ingrese la posicion en la que quiere iniciar')
    colIni = int(input('Columna: ')) - 1
    rowIni = int(input('Fila: ')) - 1
    chessboard[rowIni][colIni] = 1

    solve(colIni, rowIni)


#Resolver
def solve(colIni, rowIni):
    global chessboard
    future = 0
    possiblePositions(colIni, rowIni, future)


def possiblePositions(colIni, rowIni, future):
    countIter = 0
    listWay = []
    listBestP = []
    theBestway = {}
    count = 0
    switcher = {
        1: {
                'col': colIni + 2,
                'row': rowIni - 1
            },
        2: {
                'col': colIni + 2,
                'row': rowIni + 1
            },
        3: {
                'col': colIni - 2,
                'row': rowIni - 1
            },
        4: {
                'col': colIni - 2,
                'row': rowIni + 1
            },
        5: {
                'row': rowIni + 2,
                'col': colIni - 1
            },
        6: {
                'row': rowIni + 2,
                'col': colIni + 1
            },
        7: {
                'row': rowIni - 2,
                'col': colIni - 1
            },
        8: {
                'row': rowIni - 2,
                'col': colIni + 1
            }
    }
    for i in range(1, 9):
        wu = limitWayToUse(switcher.get(i))
        if wu:
            if chessboard[switcher.get(i).get('col')][switcher.get(i).get('row')] == 0:
                countIter = countIter+1
                listWay .append(i)

    B = 8
    if future == 0:
        for i in listWay:
            listBestP.append(bestWay(switcher.get(i), countIter, future))
        for i in listBestP:
            if B > i.get('count'):
                B = i.get('count')
                theBestway = i
        if theBestway:
            moveHourse(theBestway)

    return {'col': colIni, 'row': rowIni, 'count': countIter}

    '''for i in listWay:
        listBestP.append(bestWay(switcher.get(i), countIter))
    print(listBestP)

    colSelect = 0
    rowSelect = 0
    countIterlist = 8
    for i in listBestP:
        if i.get('countIter') < countIterlist:
            countIterlist = i.get('countIter')
            colSelect = i.get('col')
            rowSelect = i.get('row')
    return'''


#def setPossition(countIterlist, colSelect, rowSelect):

def moveHourse(theBestway):
    global chessboard
    global count

    count = count+1

    chessboard[theBestway.get('col')][theBestway.get('row')] = count
    if isComplete():
        printChessBoard()
    solve(theBestway.get('col'), theBestway.get('row'))



def bestWay(arg, countIter, future):
    global chessboard
    future = future+1
    k = possiblePositions(arg.get('col'), arg.get('row'), future)
    return k


def limitWayToUse(argumens):
    if argumens.get('col') < 0 or argumens.get('col') > 7:
        return False
    elif argumens.get('row') < 0 or argumens.get('row') > 7:
        return False
    else:
        return True


#imprimir chessBoard
def printChessBoard():
    global chessboard
    for i in chessboard:
        print(i)

#El caballo recorrrio el tablero
def isComplete():
    for row in chessboard:
        for col in row:
            if(col == 0):
                return False
    return True


main()